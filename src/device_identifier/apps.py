from django.apps import AppConfig


class DeviceIdetifierConfig(AppConfig):
    name = 'device_idetifier'
