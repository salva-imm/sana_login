from .views import ResendCode
from django.conf.urls import url


urlpatterns = [
    url(r'^resend/$', ResendCode.as_view(),name="resend-code"),
]