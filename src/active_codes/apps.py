from django.apps import AppConfig


class ActiveCodesConfig(AppConfig):
    name = 'active_codes'
