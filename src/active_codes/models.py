from django.db import models
from cuser.models import User
from datetime import date,datetime,timedelta

# Create your models here.

class ActiveCodes(models.Model):
    user = models.ForeignKey(User)
    code = models.CharField(default='',max_length=256)
    generate_time = models.DateTimeField(auto_now_add=False,auto_now=False,default=datetime.now)