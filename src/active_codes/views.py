from django.shortcuts import render
from django.db.models import Q,F
from rest_framework.generics import CreateAPIView
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

import random
import hashlib


from cuser.models import User
from device_identifier.models import DeviceId
from .models import ActiveCodes
from datetime import date,datetime,timedelta
import json
# Create your views here.


class ResendCode(CreateAPIView):
    renderer_classes = (JSONRenderer,)
    parser_classes = (JSONParser,)

    def post(self, request ,*args, **kwargs):
        instance = request.data
        code = random.randint(999, 10000)
        token = hashlib.sha256().hexdigest()
        try:
            user = User.objects.get(phone=instance['phone'])
            device = DeviceId.objects.get(
                id=user.device.id,
            )
            if not device.check_restricted():
                return Response({'state':False,'msg':'your device has been restricted'}) 
            count = ActiveCodes.objects.filter(user__phone=instance['phone']).count()
            user = User.objects.get(phone=instance['phone'])
            if count%3==0:
                if user.check_restricted():
                    ActiveCodes.objects.create(
                        user=user,
                        code=code
                    )
                    return Response({'state':True,'code':code})
                else:
                    return Response({'state':False,'msg':'You restricted'})
            else:
                ActiveCodes.objects.create(
                        user=user,
                        code=code
                    )
                return Response({'state':True,'code':code})
        except:
            return Response({'state':False,'msg':'You Didnt Register Yet'})