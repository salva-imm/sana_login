from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User as Old
from .models import User

class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ( 'phone','fullname','token',
                     'is_admin','password','device',
                     'is_active','is_restricted','restrict_untill')
    list_filter = ('is_admin',)
    ##EditUser
    fieldsets = (
        (None, {'fields': ('phone', 'password','token',
                     'is_admin','fullname','device',
                     'is_active','is_restricted','restrict_untill','login_wrong_pass',
                    'login_restrict','login_restrict_untill')}),
        ('Personal info', {'fields': ('phone',)}),
        ('Permissions', {'fields': ('is_admin',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    #CreateUser
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'password','token',
                     'is_admin','fullname','device',
                     'is_active','is_restricted','restrict_untill','login_wrong_pass',
                    'login_restrict','login_restrict_untill')}
        ),
    )
    search_fields = ('phone',)
    ordering = ('phone',)
    filter_horizontal = ()
# # Register your models here.

admin.site.register(User,UserAdmin)
admin.site.unregister(Group)
# admin.site.unregister(Old)