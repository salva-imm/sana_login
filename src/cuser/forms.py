from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User

class UserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('phone', 'password','token',
                     'is_admin','password','device',
                     'is_active','is_restricted')

class UserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('phone', 'password','token',
                     'is_admin','password','device',
                     'is_active','is_restricted')