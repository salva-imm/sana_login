from django.conf.urls import url
from .views import UserCreate,UserActivate,UserLogin,UserLogout,UserCkeckLogin


urlpatterns = [
    url(r'^create/$', UserCreate.as_view(),name="user-create"),
    url(r'^activate/$', UserActivate.as_view(),name="user-activate"),
    url(r'^login/$', UserLogin.as_view(),name="user-login"),
    url(r'^logout/$', UserLogout.as_view(),name="user-logout"),
    url(r'^check-login/$', UserCkeckLogin.as_view(),name="user-chec-login"),
]